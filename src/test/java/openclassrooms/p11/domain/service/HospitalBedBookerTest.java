package openclassrooms.p11.domain.service;

import openclassrooms.p11.domain.entity.*;
import openclassrooms.p11.domain.exception.NoBedAvailableException;
import openclassrooms.p11.domain.repository.BedRepository;
import openclassrooms.p11.domain.value_object.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HospitalBedBookerTest {

    @InjectMocks
    private HospitalBedBooker hospitalBedBooker;

    @Mock
    private BedRepository bedRepository;

    private EmergencyEntry entry;

    @BeforeEach
    public void setUp() {
        Patient patient = new Patient("John", "Doe", LocalDate.parse("1988-11-24"));
        entry = new EmergencyEntry(patient, new Position(90, 90), new Speciality("Cardiology"));
    }

    @Test
    public void testBedBookingWithAvailableBed() {
        // GIVEN
        Hospital hospital = new Hospital("Prinst", new Position(90, 90));
        Bed bed = new Bed(hospital);
        hospital.addBed(bed);

        // WHEN
        hospitalBedBooker.book(entry, hospital);

        // THEN
        long totalAvailableBedCount = hospital.getBeds().stream().filter(b -> !bed.isReserved()).count();
        long totalReservedBedCount = hospital.getBeds().stream().filter(b -> bed.isReserved()).count();
        assertEquals(0, totalAvailableBedCount);
        assertEquals(1, totalReservedBedCount);
    }

    @Test
    public void testBedBookingWithoutAvailableBed() {
        Hospital hospital = new Hospital("Prinst", new Position(90, 90));
        Bed bed = mock(Bed.class);
        when(bed.isReserved()).thenReturn(true);
        hospital.addBed(bed);

        assertThrows(NoBedAvailableException.class, () -> {
            hospitalBedBooker.book(entry, hospital);
        });
    }
}
