package openclassrooms.p11.domain.value_object;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PositionTest {

    @Test
    public void testCreateValidPosition() {
        Position position = new Position(120.7, -40.1);
        assertInstanceOf(Position.class, position);
        assertEquals(120.7, position.getLongitude());
        assertEquals(-40.1, position.getLatitude());
    }

    @Test
    public void testCreateInvalidLongitudePosition() {
        assertThrows(IllegalArgumentException.class, () -> new Position(180.1, -40));
    }

    @Test
    void testCreateInvalidLatitudePosition() {
        assertThrows(IllegalArgumentException.class, () -> new Position(120, 90.1));
    }

}
