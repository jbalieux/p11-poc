package openclassrooms.p11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@Transactional
public class P11Application {

	public static void main(String[] args) {
		SpringApplication.run(P11Application.class, args);
	}

}
