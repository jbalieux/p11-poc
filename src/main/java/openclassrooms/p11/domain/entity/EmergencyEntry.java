package openclassrooms.p11.domain.entity;

import jakarta.persistence.*;
import openclassrooms.p11.domain.value_object.Position;

import java.time.LocalDate;

@Entity
@Table
public class EmergencyEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private LocalDate createdAt;

    @Embedded
    private Position position;

    @ManyToOne
    @JoinColumn(name = "medical_needs_id")
    private Speciality medicalNeeds;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public EmergencyEntry(Patient patient, Position position, Speciality medicalNeeds) {
        this.createdAt = LocalDate.now();
        this.patient = patient;
        this.position = position;
        this.medicalNeeds = medicalNeeds;
    }

    private EmergencyEntry() { }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Speciality getMedicalNeeds() {
        return medicalNeeds;
    }

    public Patient getPatient() {
        return patient;
    }
}
