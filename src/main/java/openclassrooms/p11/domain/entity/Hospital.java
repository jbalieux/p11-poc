package openclassrooms.p11.domain.entity;

import jakarta.persistence.*;
import openclassrooms.p11.domain.exception.NoBedAvailableException;
import openclassrooms.p11.domain.value_object.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table
public class Hospital {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Embedded
    private Position position;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<Speciality> specialities = new ArrayList<>();

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<Bed> beds;

    public Hospital(String name, Position position) {
        this.name = name;
        this.position = position;
        this.beds = new ArrayList<>();
    }

    private Hospital() { }

    public String getName() {
        return name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<Speciality> specialities) {
        this.specialities = specialities;
    }

    public List<Bed> getBeds() {
        return beds;
    }

    public Bed getOneAvailableBed() {
        Optional<Bed> bed = beds.stream().filter(b -> !b.isReserved()).findFirst();
        if (bed.isEmpty()) {
            throw new NoBedAvailableException("There's no available bed on this hospital at the moment");
        }
        return bed.get();
    }

    public void addBed(Bed bed) {
        this.beds.add(bed);
    }

    public boolean hasAvailableBed() {
        return this.beds.stream().anyMatch(bed -> !bed.isReserved());
    }

    public boolean containsSpeciality(Speciality speciality) {
        return this.specialities.stream().anyMatch(s1 -> s1.equals(speciality));
    }
}
