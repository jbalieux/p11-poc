package openclassrooms.p11.domain.exception;

public class NoBedAvailableException extends RuntimeException {
    public NoBedAvailableException(String message) {
        super(message);
    }
}
