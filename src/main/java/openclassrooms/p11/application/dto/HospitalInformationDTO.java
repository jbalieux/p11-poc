package openclassrooms.p11.application.dto;

import openclassrooms.p11.domain.entity.EmergencyEntry;
import openclassrooms.p11.domain.entity.Hospital;
import openclassrooms.p11.domain.value_object.Distance;

public class HospitalInformationDTO {
    public String name;
    public double longitude;
    public double latitude;
    public double distanceInKmFromYourLocation;

    public HospitalInformationDTO(Hospital hospital, EmergencyEntry entry) {
        this.name = hospital.getName();
        this.longitude = hospital.getPosition().getLongitude();
        this.latitude = hospital.getPosition().getLatitude();
        this.distanceInKmFromYourLocation = new Distance(hospital.getPosition(), entry.getPosition()).getValueInKm();
    }
}
